CREATE TABLE knight (
    id SERIAL PRIMARY KEY,
    name varchar(255) NOT NULL,
    strength varchar(255) NOT NULL,
    weapon_power varchar(255) NOT NULL
);