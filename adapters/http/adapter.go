package http

import (
	"context"
	"encoding/json"
	"fmt"
	"github.com/julienschmidt/httprouter"
	"gitlab.com/zenport.io/go-assignment/domain"
	"gitlab.com/zenport.io/go-assignment/engine"
	"log"
	"net/http"
	"time"
)

type WinnerResponse struct {
	Id string `json:"id"`
}

type KnightResponse struct {
	Id          string `json:"id"`
	Name        string `json:"name"`
	Strength    int    `json:"strength"`
	WeaponPower int    `json:"weapon_power"`
}

type ErrorResponse struct {
	Code    int    `json:"code"`
	Message string `json:"message"`
}

type HTTPAdapter struct {
	server *http.Server
	router *httprouter.Router
}

func (adapter *HTTPAdapter) Start() {
	log.Printf("Httpserver: ListenAndServe() starting")
	go func() {
		if err := adapter.server.ListenAndServe(); err != nil {
			log.Printf("Httpserver: ListenAndServe() error: %s", err)
		}
	}()
}

func (adapter *HTTPAdapter) Stop() {
	log.Printf("Httpserver: ListenAndServe() closing")
	ctx, cancel := context.WithTimeout(context.Background(), time.Second*10)
	defer cancel()
	if err := adapter.server.Shutdown(ctx); err != nil {
		panic(err)
	}
}

func NewHTTPAdapter(e engine.Engine) *HTTPAdapter {
	router := httprouter.New()
	router.GET("/knight/:id", func(w http.ResponseWriter, r *http.Request, ps httprouter.Params) {
		GetKnight(w, r, ps, e)
	})
	router.GET("/knight", func(w http.ResponseWriter, r *http.Request, ps httprouter.Params) {
		ListKnights(w, r, ps, e)
	})
	router.POST("/knight", func(w http.ResponseWriter, r *http.Request, ps httprouter.Params) {
		CreateKnight(w, r, ps, e)
	})
	router.GET("/fight/:id1/:id2", func(w http.ResponseWriter, r *http.Request, ps httprouter.Params) {
		KnightFight(w, r, ps, e)
	})

	server := &http.Server{
		Handler: router,
		Addr:    ":8080",
	}

	return &HTTPAdapter{server: server, router: router}
}

func GetKnight(w http.ResponseWriter, r *http.Request, ps httprouter.Params, e engine.Engine) {
	w.Header().Set("Content-Type", "application/json; charset=UTF-8")

	knight, err := e.GetKnight(ps.ByName("id"))
	if err != nil {
		HandleErrorResponse(w, http.StatusNotFound, fmt.Sprintf("Knight #%s not found.", ps.ByName("id")))
		return
	}

	knightResponse := KnightResponse{
		Id:          knight.GetID(),
		Name:        knight.GetName(),
		Strength:    knight.GetStrength(),
		WeaponPower: knight.GetWeaponPower(),
	}

	w.WriteHeader(http.StatusOK)
	if err := json.NewEncoder(w).Encode(knightResponse); err != nil {
		log.Printf("impossible to encode knightResponse error: %q", err)
	}

}

func ListKnights(w http.ResponseWriter, r *http.Request, ps httprouter.Params, e engine.Engine) {
	w.Header().Set("Content-Type", "application/json; charset=UTF-8")

	knights := e.ListKnights()
	var listKnightsResponse []KnightResponse
	for _, knight := range knights {
		knightResponse := KnightResponse{
			Id:          knight.GetID(),
			Name:        knight.GetName(),
			Strength:    knight.GetStrength(),
			WeaponPower: knight.GetWeaponPower(),
		}

		listKnightsResponse = append(listKnightsResponse, knightResponse)
	}

	w.WriteHeader(http.StatusOK)
	if err := json.NewEncoder(w).Encode(listKnightsResponse); err != nil {
		log.Printf("impossible to encode listKnightsResponse error: %q", err)
	}
}

func CreateKnight(w http.ResponseWriter, r *http.Request, ps httprouter.Params, e engine.Engine) {
	decoder := json.NewDecoder(r.Body)

	knightResponse := KnightResponse{}
	err := decoder.Decode(&knightResponse)
	if err != nil {
		HandleErrorResponse(w, http.StatusBadRequest, "Not a Json payload.")
		return
	}

	if knightResponse.Name == "" || knightResponse.Strength == 0 || knightResponse.WeaponPower == 0 {
		HandleErrorResponse(w, http.StatusBadRequest, "bad parameters")
		return
	}

	knight := domain.Knight{}
	knight.SetName(knightResponse.Name)
	knight.SetStrength(knightResponse.Strength)
	knight.SetWeaponPower(knightResponse.WeaponPower)

	e.SaveKnight(&knight)

	w.WriteHeader(http.StatusCreated)
}

func KnightFight(w http.ResponseWriter, r *http.Request, ps httprouter.Params, e engine.Engine) {
	knight, err := e.Fight(ps.ByName("id1"), ps.ByName("id2"))
	if err != nil {
		HandleErrorResponse(w, http.StatusNotFound, err.Error())
		return
	}

	winnerResponse := WinnerResponse{}
	if knight != nil {
		winnerResponse.Id = knight.GetID()
	}

	if err := json.NewEncoder(w).Encode(winnerResponse); err != nil {
		log.Printf("impossible to encode winnerResponse error: %q", err)
	}
}

func HandleErrorResponse(w http.ResponseWriter, status int, message string) {
	errorResponse := ErrorResponse{
		Code:    status,
		Message: message,
	}

	w.WriteHeader(errorResponse.Code)
	if err := json.NewEncoder(w).Encode(errorResponse); err != nil {
		log.Printf("impossible to encode errorResponse error: %q", err)
	}
}
