package domain

type Fighter interface {
	GetID() string
	GetPower() float64
}

type Knight struct {
	id          string
	name        string
	strength    int
	weaponPower int
}

func (knight *Knight) GetID() string {
	return knight.id
}

func (knight *Knight) GetPower() float64 {
	return float64(knight.strength * knight.weaponPower)
}

func (knight *Knight) GetWeaponPower() int {
	return knight.weaponPower
}

func (knight *Knight) GetStrength() int {
	return knight.strength
}

func (knight *Knight) GetName() string {
	return knight.name
}

func (knight *Knight) SetWeaponPower(weaponPower int) {
	knight.weaponPower = weaponPower
}

func (knight *Knight) SetStrength(strength int) {
	knight.strength = strength
}

func (knight *Knight) SetID(id string) {
	knight.id = id
}

func (knight *Knight) SetName(name string) {
	knight.name = name
}
