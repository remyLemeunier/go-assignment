package domain

type Arena struct{}

// The function Fight will compare two fighters powers.
// Return the best Fighter or nil if they are even.
func (arena *Arena) Fight(fighter1 Fighter, fighter2 Fighter) Fighter {
	if fighter1.GetPower() == fighter2.GetPower() {
		return nil
	}

	if fighter1.GetPower() > fighter2.GetPower() {
		return fighter1
	}

	return fighter2
}
