package engine

import (
	"gitlab.com/zenport.io/go-assignment/domain"
	"os"
	"testing"
)

var engine arenaEngine

var knight1, knight2, knight3 domain.Knight

func TestMain(m *testing.M) {
	engine = arenaEngine{
		arena:            &domain.Arena{},
		knightRepository: &knightRepositoryMock{},
	}

	knight1 = domain.Knight{}
	knight1.SetID("1")
	knight1.SetStrength(1)
	knight1.SetWeaponPower(2)

	knight2 = domain.Knight{}
	knight2.SetID("2")
	knight2.SetStrength(2)
	knight2.SetWeaponPower(2)

	knight3 = domain.Knight{}
	knight3.SetID("3")
	knight3.SetStrength(2)
	knight3.SetWeaponPower(1)

	code := m.Run()
	os.Exit(code)
}

func TestGetKnight(t *testing.T) {
	knight1, err := engine.GetKnight("1")
	if err != nil {
		t.Fatalf("Error raised %q", err)
	}

	if knight1.GetStrength() != 1 {
		t.Errorf("Strength should be 1 instead got %d", knight1.GetStrength())
	}

	if knight1.GetWeaponPower() != 2 {
		t.Errorf("Weapon power should be 2 instead got %d", knight1.GetWeaponPower())
	}

	if knight1.GetID() != "1" {
		t.Errorf("id should monster instead got %q", knight1.GetID())
	}

}

func TestGetKnightNotFound(t *testing.T) {
	_, err := engine.GetKnight("4")
	if err == nil {
		t.Fatal("Knight 4 doesn't exist, an error should have been raised.")
	}
}

func TestListKnights(t *testing.T) {
	knights := engine.ListKnights()

	if len(knights) != 3 {
		t.Fatalf("listKnights should have returned 3 knights instead got %d", len(knights))
	}
}

func TestFightWinner(t *testing.T) {
	winner, err := engine.Fight("1", "2")
	if err != nil {
		t.Fatalf("Error raised %q", err)
	}

	if winner.GetID() != "2" {
		t.Errorf("id should 2 instead got %q", winner.GetID())
	}
}

func TestFightDraw(t *testing.T) {
	draw, err := engine.Fight("1", "3")
	if err != nil {
		t.Fatalf("Error raised %q", err)
	}

	if draw != nil {
		t.Error("Fighters have equal power and it sould have been a draw.")
	}
}

func TestFightFighterNotFound(t *testing.T) {
	_, err := engine.Fight("1", "4")
	if err == nil {
		t.Fatal("Knight 4 doesn't exist, an error should have been raised.")
	}
}

type knightRepositoryMock struct{}

func (repository *knightRepositoryMock) Find(ID string) *domain.Knight {
	if ID == "1" {
		return &knight1
	}

	if ID == "2" {
		return &knight2
	}

	if ID == "3" {
		return &knight3
	}

	return nil
}

func (repository *knightRepositoryMock) FindAll() []*domain.Knight {
	knights := []*domain.Knight{}
	knights = append(knights, &knight1, &knight2, &knight3)

	return knights
}

func (repository *knightRepositoryMock) Save(knight *domain.Knight) {}
