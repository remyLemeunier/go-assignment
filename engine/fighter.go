package engine

import (
	"errors"
	"fmt"

	"gitlab.com/zenport.io/go-assignment/domain"
)

func (engine *arenaEngine) GetKnight(ID string) (*domain.Knight, error) {
	fighter := engine.knightRepository.Find(ID)
	if fighter == nil {
		return nil, errors.New(fmt.Sprintf("fighter with ID '%s' not found!", ID))
	}

	return fighter, nil
}

func (engine *arenaEngine) ListKnights() []*domain.Knight {
	return engine.knightRepository.FindAll()
}

// Return the best fighter or an error if a fighter has not been found.
func (engine *arenaEngine) Fight(fighter1ID string, fighter2ID string) (domain.Fighter, error) {
	fighter1, err := engine.GetKnight(fighter1ID)
	if err != nil {
		return nil, err
	}

	fighter2, err := engine.GetKnight(fighter2ID)
	if err != nil {
		return nil, err
	}

	winner := engine.arena.Fight(fighter1, fighter2)

	return winner, nil
}

func (engine *arenaEngine) SaveKnight(fighter *domain.Knight) {
	engine.knightRepository.Save(fighter)
}
