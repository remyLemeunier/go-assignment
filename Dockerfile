# My dockerfile
FROM golang:1.11 AS builder

# Download and install the latest release of dep
ADD https://github.com/golang/dep/releases/download/v0.4.1/dep-linux-amd64 /usr/bin/dep
RUN chmod +x /usr/bin/dep

WORKDIR /go/src/gitlab.com/zenport.io/go-assignment
ADD . /go//src/gitlab.com/zenport.io/go-assignment

COPY Gopkg.toml Gopkg.lock ./
RUN dep ensure --vendor-only