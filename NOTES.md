# Notes

*Database setup, structure change, tests setup, etc.*

#How to setup the project
Connection to psql
```bash
sudo -u postgres psql
```
Create the user and the database
```sql
CREATE USER arena_battle_user WITH PASSWORD 'arena_battle_password';
CREATE DATABASE arena_battle;
```
Connection to the database
```bash
psql -U arena_battle_user arena_battle
```
Create the table
```sql
CREATE TABLE knight (
    id SERIAL PRIMARY KEY,
    name varchar(255) NOT NULL,
    strength varchar(255) NOT NULL,
    weapon_power varchar(255) NOT NULL
);
```
#How to setup the project
```bash
dep ensure
go build -o /tmp/battle
cd /tmp
./battle
curl -v 127.0.0.1:8080/knight
```
#How to launch tests
```bash
go test -p 1 ./domain ./providers/database ./adapters/http
```
#Docker
Docker is working, you have to change the host to db in provider. But i didn't found how to create a table when launching it.
- Add in sqlx.Connect in provider host=db
```bash
docker build . -t go-assignment
```
```bash
docker-compose up
```
Find the psql container
```bash
docker ps
```
Enter in the psql container
```bash
docker exec -it xxxx bash

```
Connection to the database
```bash
psql -U arena_battle_user arena_battle
```
Create the table
```sql
CREATE TABLE knight (
    id SERIAL PRIMARY KEY,
    name varchar(255) NOT NULL,
    strength varchar(255) NOT NULL,
    weapon_power varchar(255) NOT NULL
);
```