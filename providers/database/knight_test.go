package database

import (
	"gitlab.com/zenport.io/go-assignment/domain"
	"log"
	"os"
	"testing"
)

var provider *Provider

func TestMain(m *testing.M) {
	var err error
	provider, err = NewProvider()
	if err != nil {
		log.Fatal(err)
	}

	provider.Db.Exec("DROP TABLE knight")

	provider.Db.Exec(`CREATE TABLE knight (
		id SERIAL PRIMARY KEY,
		name varchar(255) NOT NULL,
		strength varchar(255) NOT NULL,
		weapon_power varchar(255) NOT NULL
	);`)

	code := m.Run()
	provider.Close()
	os.Exit(code)
}

func TestSave(t *testing.T) {
	knight := domain.Knight{}
	knight.SetName("monster")
	knight.SetWeaponPower(2)
	knight.SetStrength(1)

	provider.GetKnightRepository().Save(&knight)

	knight2 := domain.Knight{}
	knight2.SetName("big")
	knight2.SetWeaponPower(10)
	knight2.SetStrength(20)

	provider.GetKnightRepository().Save(&knight2)
}

func TestFindAll(t *testing.T) {
	knights := provider.GetKnightRepository().FindAll()

	if len(knights) != 2 {
		t.Fatalf("knights length should be 2 instead got %d", len(knights))
	}

	if knights[0].GetStrength() != 1 {
		t.Errorf("Strength should be 1 instead got %d", knights[0].GetStrength())
	}

	if knights[0].GetWeaponPower() != 2 {
		t.Errorf("Weapon power should be 2 instead got %d", knights[0].GetWeaponPower())
	}

	if knights[0].GetName() != "monster" {
		t.Errorf("id should monster instead got %q", knights[0].GetName())
	}

	if knights[1].GetStrength() != 20 {
		t.Errorf("Strength should be 20 instead got %d", knights[1].GetStrength())
	}

	if knights[1].GetWeaponPower() != 10 {
		t.Errorf("Weapon power should be 10 instead got %d", knights[1].GetWeaponPower())
	}

	if knights[1].GetName() != "big" {
		t.Errorf("id should monster instead got %q", knights[1].GetName())
	}
}

func TestFind(t *testing.T) {
	knight := provider.GetKnightRepository().Find("1")
	if knight == nil {
		t.Fatal("Knight not found")
	}

	if knight.GetStrength() != 1 {
		t.Errorf("Strength should be 1 instead got %d", knight.GetStrength())
	}

	if knight.GetWeaponPower() != 2 {
		t.Errorf("Weapon power should be 2 instead got %d", knight.GetWeaponPower())
	}

	if knight.GetName() != "monster" {
		t.Errorf("id should monster instead got %q", knight.GetID())
	}
}

func TestFindNone(t *testing.T) {
	knight := provider.GetKnightRepository().Find("11345678")
	if knight != nil {
		t.Fatal("Knight found but doesn't exist.")
	}
}
