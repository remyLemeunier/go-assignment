package database

import (
	"github.com/jmoiron/sqlx"
	_ "github.com/lib/pq"
	"gitlab.com/zenport.io/go-assignment/engine"
)

type Provider struct {
	Db *sqlx.DB
}

func (provider *Provider) GetKnightRepository() engine.KnightRepository {
	return &knightRepository{provider.Db}
}

func (provider *Provider) Close() {
	provider.Db.Close()
}

func NewProvider() (*Provider, error) {
	db, err := sqlx.Connect("postgres", "user=arena_battle_user password=arena_battle_password dbname=arena_battle sslmode=disable") // host=db
	if err != nil {
		return nil, err
	}

	return &Provider{db}, nil
}
