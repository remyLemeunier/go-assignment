package database

import (
	"reflect"
	"testing"
)

func TestNewProvider(t *testing.T) {
	provider, err := NewProvider()
	if err != nil {
		t.Fatal(err)
	}

	if reflect.TypeOf(provider.Db).String() != "*sqlx.DB" {
		t.Errorf("Type should be *sqlx.DB instead got %q", reflect.TypeOf(provider.Db).String())
	}
}
