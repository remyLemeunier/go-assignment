package database

import (
	"github.com/jmoiron/sqlx"
	"gitlab.com/zenport.io/go-assignment/domain"
	"log"
)

type knightRepository struct {
	Db *sqlx.DB
}

func (repository *knightRepository) Find(ID string) *domain.Knight {
	row := repository.Db.QueryRow("SELECT id, name, strength, weapon_power FROM knight WHERE id=$1", ID)

	var id, name string
	var strength, weaponPower int
	err := row.Scan(&id, &name, &strength, &weaponPower)
	// If no row scan return an error
	if err != nil {
		return nil
	}

	knight := domain.Knight{}
	knight.SetID(id)
	knight.SetName(name)
	knight.SetStrength(strength)
	knight.SetWeaponPower(weaponPower)

	return &knight
}

func (repository *knightRepository) FindAll() []*domain.Knight {
	knights := []*domain.Knight{}
	rows, err := repository.Db.Query("SELECT id, name, strength, weapon_power FROM knight")
	if err != nil {
		log.Printf(err.Error())
		return knights
	}

	for rows.Next() {
		var id, name string
		var strength, weaponPower int
		err := rows.Scan(&id, &name, &strength, &weaponPower)
		if err != nil {
			log.Printf(err.Error())
			return knights
		}

		knight := domain.Knight{}
		knight.SetID(id)
		knight.SetName(name)
		knight.SetStrength(strength)
		knight.SetWeaponPower(weaponPower)

		knights = append(knights, &knight)
	}

	return knights
}

func (repository *knightRepository) Save(knight *domain.Knight) {
	_, err := repository.Db.NamedExec(`INSERT INTO knight(name, strength,weapon_power) VALUES (:name, :strength, :weapon_power)`,
		map[string]interface{}{
			"name":         knight.GetName(),
			"strength":     knight.GetStrength(),
			"weapon_power": knight.GetWeaponPower(),
		})

	if err != nil {
		log.Printf(err.Error())
	}
}
