# Questions

For this assignment you also have to answer a couple of questions.
There is no correct answer and none is mandatory, if you don't know just skip it.

 - **What do you think of the initial project structure ?**
i liked the project structure but i think it's a bit too based on Knight and not enough portable 
for other kind of Fighters. It's a domain driven design which i think is great for services.
 - **What you will improve from your solution ?**
    - Add a yaml configuration file instead of raw configuration.
    - Add a travis (or any CI) file, it will run tests for each push made on the repository.
    - listKnights should have a limit. Retrieving the whole database is dangerous. So i would add limit offset.
    - Better error handling, especially on the database part. Return error 500 if error with the db.
    - I changed Fight() function to handle error.
    - Find/FindAll/Saved don't return error, i would have added this as mentionned earlier. I didn't wanted to change the Interface.
    - SaveKnight has been added in the Interface in order to use it.
    - I was also a bit confused with ID as string. (Maybe change it to int ?)
    - I have also added the endpoint Fight. It wasn't mentioned in the test.
 - **For you, what are the boundaries of a service inside a micro-service architecture ?**
I'm not sure i understood the question well. But the main problem of micro-service architecture is it take
a lot of time to maintain the different services. When you update a service you have to think about backward
compatibility. As you have more services you have more chance of problems (network, db, etc..).    
 - **For you, what are the most relevant usage for SQL, NoSQL, key-value and document store ?**
    - Sql: Structured data architecture without a lot of changes. Consistency is a plus. A lot of check are done one
    the data part. (int, string, etc) 
    - NoSql: Opposite of Sql, no structured data and data that are changing a lot. Drawback is that data are not super clean compared to Sql.
    - Key/Value: I would use it only for caching purpose or lock. Not to been seen as a primary database.
    - document store: Also when data change often. I would use it for searching purpose.


